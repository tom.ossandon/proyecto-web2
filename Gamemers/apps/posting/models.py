from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.

class Usuario(models.Model):
    id_usuario = models.CharField(max_length=30,primary_key=True)
    nombre = models.CharField(max_length=50,null=False)
    nick = models.CharField(max_length=16,null=False)
    fecha_nac = models.DateField()
    biografia = models.CharField(max_length=500,blank=True)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='')
    def __str__(self):
        return f'perfil de {self.user.username}'

class Comentario(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE,related_name='comentarios')
    id_comentario = models.IntegerField(primary_key=True)
    comentario = models.TextField(max_length=200)
    fecha_comentario = models.DateTimeField(default=timezone.now)
    
    

    def __str__(self):
        return f'{self.user.username}: {self.comentario}'

class Categoria(models.Model):
    id_categoria = models.IntegerField(primary_key=True)
    nom_categoria = models.CharField(max_length=20)
    


class Post(models.Model):
    id_post = models.IntegerField(primary_key=True)
    fecha_post = models.DateField()
    titulo = models.CharField(max_length=10)
    imagen = models.ImageField(upload_to='posts')
    nombre_img= models.CharField(max_length=20)
    Usuario = models.ForeignKey(Usuario,null=False,blank=False,on_delete = models.CASCADE)
    Categoria = models.ForeignKey(Categoria,null=False,blank=False,on_delete = models.CASCADE)

