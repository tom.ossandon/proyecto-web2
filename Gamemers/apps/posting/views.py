from django.shortcuts import render, redirect,get_object_or_404
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib import messages
from .models import *
from .forms import UserRegisterForm, ComenForm

# Create your views here.
def inicio(request):
    return render(request,'posting/index.html')

def articulo_1(request):
    coments = Comentario.objects.all()

    context = {'coments':coments}
    return render(request, 'posting/article.html', context)

def articulo_2(request):
    return render(request, 'posting/article2.html')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            messages.success(request, f'Usuario {username} creado')
            return redirect('inicio')
    else:
        form = UserCreationForm()

    context = { 'form' : form }
    return render(request, 'posting/register.html', context)

def comentario(request):
    current_user = get_object_or_404(User, pk=request.user.pk)
    if request.method == 'POST':
        form = ComenForm(request.POST)
        if form.is_valid():
            comentario = form.save(commit=False)
            comentario.user=current_user
            comentario.save()
            messages.success(request,'Comentario Publicado')
            return redirect('articulo_1')
    else:
        form = ComenForm()
    return render(request, 'posting/comentario.html',{'form' : form})

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            messages.success(request, f'Usuario {username} creado')
            return redirect('inicio')
    else:
        form = UserRegisterForm()

    context = { 'form' : form }
    return render(request, 'posting/register.html', context)
    
    