from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Comentario

class ComenForm(forms.ModelForm):
    comentario = forms.CharField(label='',widget=forms.Textarea(attrs={'rows':2,'placedholder':'Haz tu comentario aqui'}),required=True)

    class Meta:
        model= Comentario
        fields= ['comentario']

class UserRegisterForm(UserCreationForm):
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='confirmar contraseña', widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']
        help_texts = {k:"" for k in fields }