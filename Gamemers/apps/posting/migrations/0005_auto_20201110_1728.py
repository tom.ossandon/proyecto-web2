# Generated by Django 3.1.2 on 2020-11-10 20:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('posting', '0004_auto_20201110_1724'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comentario',
            name='Usuario',
        ),
        migrations.AddField(
            model_name='comentario',
            name='user',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='comentarios', to='auth.user'),
            preserve_default=False,
        ),
    ]
