from django.contrib import admin
from django.urls import path,include
from . import views
from django.contrib.auth.views import LoginView, LogoutView
from .views import inicio, articulo_1, articulo_2, comentario

urlpatterns = [
    path('',inicio, name="inicio"),
    path('article.html',articulo_1, name="articulo_1"),
    path('article2.html',articulo_2, name="articulo_2"),
    path('login/',LoginView.as_view(template_name='posting/login.html'),name='login'),
    path('logout/',LogoutView.as_view(template_name='posting/logout.html'),name='logout'),
    path('register/', views.register, name='register'),
    path('comentario.html', views.comentario, name='comentario'),
]