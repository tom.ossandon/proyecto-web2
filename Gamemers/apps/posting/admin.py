from django.contrib import admin
from apps.posting.models import Categoria,Usuario,Post,Comentario
# Register your models here.
admin.site.register(Categoria)
admin.site.register(Post)
admin.site.register(Comentario)
admin.site.register(Usuario)